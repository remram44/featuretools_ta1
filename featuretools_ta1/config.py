from d3m import utils as d3m_utils
from d3m.metadata import base as metadata_base
from pathlib import Path


AUTHOR = "MIT_FeatureLabs"
CONTACT = "mailto:alice.r.yepremyan@jpl.nasa.gov"


_git_commit = d3m_utils.current_git_commit(Path(__file__).parents[1])
INSTALLATION = [{
    'type': metadata_base.PrimitiveInstallationType.PIP,
    'package_uri': 'git+https://gitlab.com/yepremyana/featuretools_ta1.git@{git_commit}#egg=featuretools_ta1'.format(
        git_commit=_git_commit)}]
